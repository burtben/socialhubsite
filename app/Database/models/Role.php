<?php

use App\Database\models\Model;

class Role extends Model {
    protected $_fillable = ['role_name', 'role_permissions'];
    
}