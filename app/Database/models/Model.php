<?php

namespace App\Database\models;

use DB;

class Model {

    protected $_table = null;
    protected $_id = null;
    protected $_db = null;
    protected $_fillable = [];
    protected $_currentVars = [];
    protected $_mode = "SELECT";
    protected $_instance = null;

    public function __construct($id = null) {
        if($this->_table == null) {
            $class_name = explode('\\', get_class($this));
            $this->_table = strtolower($this->pluralize(2,end($class_name)));
        }
        if($this->_id == null) {
            $this->_id = "id";
        }
        if($this->_db == null) {
            $this->_db = DB::table($this->_table);
        }
        if($id == null) {
            foreach ($this->_db->getTableColumns() as $key => $value) {
                $this->_currentVars[$value] = null;
                $this->$value = null;
            }
        } else {
            $findResult = $this->_db->where($this->_id, '=', $id);
            if($this->_db->count() > 0) {
            $vars = $findResult->first();
            foreach ($vars as $key => $value) {
                $this->_currentVars[$key] = $value;
                $this->$key = $value;
            }
        }
    }
    }

    public static function find($id) {
        $in = new static();
        $findResult = $in->_db->where($in->_id, '=', $id);
        if($in->_db->count() > 0) {
            $vars = $findResult->first();
            foreach ($vars as $key => $value) {
                $in->$key = $value;
            }
            return $in;
        } else {
            return 'No record found with the id of: ' . $id;
        }
    }

    public static function create() {
        $in = new static();
        $in->_mode = "CREATE";
        return $in;
    }

    public static function update($key, $operator = '=', $value) {
        $in = new static();
        $in->_mode = 'UPDATE';
        $in->_db->where($key, $operator, $value);
        return $in;
    }

    public function save() {
        switch($this->_mode) {
            case 'CREATE':
                $vars = [];
                foreach ($this->_fillable as $key => $value) {
                    if($this->$value == null) {
                         throw new RuntimeException('Not all required fields have been passed in.');
                    }
                    $vars[$value] = $this->$value;
                }
                $id = $this->_db->insertGetId($vars);
                $this->id = $id;
                break;
            case 'UPDATE':
                $vars = [];
                foreach ($this->_currentVars as $key => $value) {
                    if(($this->_currentVars[$key] != $this->$key) && in_array($key, $this->_fillable)) {
                        $vars[$key] = $this->$key;
                    }
                }
                $this->_db->update($vars);
                break;
        }
    }

    public static function all() {
        $in = new static();
        $results = $in->_db->get();
        $array = [];
        foreach ($results as $key => $value) {
            $result = $results[$key];
            array_push($array, new static($result[$in->_id]));
        }
        return $array;
    }

    public static function where($key, $operator = '=', $value) {
        $in = new static();
        $in->_db->where($key, $operator, $value);
        return $in;
    }

    public function get() {
        return $this->_db->get();
        return $this;
    }

    public function first() {
        return $this->_db->first();
        return $this;
    }

    public function count() {
        return $this->_db->count();
        return $this;
    }

    public function orderBy($column) {
        $this->_db->orderBy($column);
        return $this;
    }

    public function ASC() {
        $this->_db->ASC();
        return $this;
    }

    public function DESC() {
        $this->_db->DESC();
        return $this;
    }

    public function take($amount) {
        $this->_db->take($amount);
        return $this;
    }

    public function delete() {
        $this->get();
        $this->where($this->_id , '=', $this->id);
        $this->_db->delete();
    }

    public static function destory($id) {
        $in = new static();
        $in->get();
        $in->where($in->_id, '=', $id);
        $in->delete();
    }

    protected function belongsTo($model, $key = null) {
        $this->_db->get();
        if($key == null) {
            $columns = $this->_db->getTableColumns();
            $modelparts = explode('\\', $model);
            $column = strtolower(end($modelparts)) . '_id';
            if(in_array($column, $columns)) {
                return new $model($this->$column);
            } else {
                throw new RuntimeException("Foreign key not found.");
                
            }
        } else {
            return new $model($this->$key);
        }
    }

    protected function hasOne($model, $key = null) {
        $modelObj = new $model();
        $modelObj->_db->get();
        if($key == null) {
            $class_name = explode('\\', get_class($this));
            $columns = $modelObj->_db->getTableColumns();
            $column = strtolower(end($class_name)) . '_id';
            if(in_array($column, $columns)) {
                $models = call_user_func(array($model, 'all'));
                foreach ($models as $obj) {
                    if($obj->$column == $this->id) {
                        return $obj;
                    }
                }
            } else {
                throw new RuntimeException("Foreign key not found.");
                
            }
        } else {
            $models = call_user_func(array($model, 'all'));
                foreach ($models as $obj) {
                    if($obj->$key == $this->id) {
                        return $obj;
                    }
                }
        }
    }
    //This Model: Post, connect to Comment
    //For example a Post Model will you hasMany for Comment model so it can get all the comments connected to that post.
    protected function hasMany($model, $key = null) {
        $modelObj = new $model();
        $modelObj->_db->get();
        if($key == null) {
            $class_name = explode('\\', get_class($this));
            $columns = $modelObj->_db->getTableColumns();
            $column = strtolower(end($class_name)) . '_id';
            if(in_array($column, $columns)) {
                $models = call_user_func(array($model, 'all'));
                $array = array();
                foreach ($models as $obj) {
                    if($obj->$column == $this->id) {
                        array_push($array, $obj);
                    }
                }
                return $array;
            } else {
                throw new RuntimeException("Foreign key not found.");
                
            }
        } else {
            $models = call_user_func(array($model, 'all'));
            $array = array();
            foreach ($models as $obj) {
                if($obj->$key == $this->id) {
                    array_push($array, $obj);
                }
            }
            return $array;
        }
    }

    protected function belongsToMany($model, $manyTable = null) {
        $modelparts = explode('\\',$model);
        $modelName = strtolower(end($modelparts));
        $class_name = explode('\\', get_class($this));
        $namesSortArray = array($modelName, strtolower(end($class_name)));
        sort($namesSortArray);
        if($manyTable == null) {
            $manyTable = $namesSortArray[0] . '_' . $namesSortArray[1];
        }
        $results = DB::table($manyTable)->where(strtolower(end($class_name)) . '_id', '=', $this->id)->get();
        $array = [];
        foreach ($results as $key => $value) {
            $model_id = $results[$key][$modelName . '_id'];
            array_push($array, new $model($model_id));
        }
        return $array;
    }
    //Looking for all Posts(Model One) with the current Model's country_id(this) through the users(Model Two) table.
    protected function hasManyThrough($modelOne, $modelTwo, $keyForModelTwo = null, $key = null) {
        $modelTwoArray = $this->hasMany($modelTwo, $key);
        $modelOneArray = array();
        foreach ($modelTwoArray as $modelTwoObj) {
            array_push($modelOneArray, $modelTwoObj->hasMany($modelOne, $keyForModelTwo));
        }
        return $array;
    }




    private function pluralize($quantity, $singular, $plural=null) {
        if($quantity==1 || !strlen($singular)) return $singular;
        if($plural!==null) return $plural;

        $last_letter = strtolower($singular[strlen($singular)-1]);
        switch($last_letter) {
            case 'y':
                return substr($singular,0,-1).'ies';
            case 's':
                return $singular.'es';
            default:
                return $singular.'s';
        }
    }

}