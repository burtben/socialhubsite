<?php

use App\Database\models\Model;

class Info extends Model {
    protected $_table = 'info';
    protected $_fillable = ['name', 'city', 'town'];

    public function user() {
        return $this->belongsTo('User');
    }
}