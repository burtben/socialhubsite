<?php

use App\Database\models\Model;

class User extends Model {
    protected $_fillable = ['username', 'password', 'email'];

    public function info() {
        return $this->hasOne('Info');
    }

    public function roles() {
        return $this->belongsToMany('Role');
    }
}