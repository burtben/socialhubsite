<?php

namespace App\Database;

use Config;
use \PDO;
use Collection;

class DB {
    
    private $table = null;
    private $db = null;
    private $statement;
    private $queryString = "";
    private $bindAttribs = array();
    private $whereCount = 0;
    private $whereStatement = "";
    
    /**
     * This sets up a connection to the database
     * @param String $table this is used for specifying what table you want to apply a statement to.
     */
    private function __construct($table) {
        //Details for the database
        $dbdet = 'mysql:host=' . Config::get('database/host') . ';dbname=' . Config::get('database/database');

        //Options for the PDO.
        $options = [PDO::ATTR_PERSISTENT => true, PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION];

        //create the PDO object.
        $this->db = new PDO($dbdet, Config::get('database/username'), Config::get('database/password'));

        $this->table = $table;

        $this->queryString = 'SELECT * FROM ' . $table;

    }
    /**
     * This is to get a instance of the class by table
     * @param  String     $table The table name goes here.
     * @return Object(DB)        The instance of the DB class is returned.
     */
    public static function table($table) {
        $in = new DB($table);
        return $in;
    }

    /**
     * This is a function to allow you to create your own sql statement but be aware that this soes not support any sql injection protection.
     * @param  String     $sql The sql you want to run goes here.
     * @return Object(DB)      The instance of the DB class is returned.
     */
    public static function sql($sql) {
        $in = new DB('');
        $in->queryString = $sql;
        $in->execute();
        return $in;
    }
    
    /**
     * Prepares the query for execution.
     * @param  String $query the SQL statement goes here.
     */
    private function query($query) {
        $this->statement = $this->db->prepare($query);
    }

    /**
     * This is used to the SQL Staement.
     */
    private function execute() {
        $this->query($this->queryString);
        $this->whereCount = 0;
        $this->whereStatement = "";
        $this->queryString = 'SELECT * FROM ' . $this->table;
        $bind = $this->bindAttribs;
        $this->bindAttribs = array();
        return $this->statement->execute($bind);
    }

    /**
     * This will return an array of the results found.
     * @return Array an array of the results found in a asscociate array so basically a dictionary.
     */
    public function get() {
        $this->execute();
        return $this->statement->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * This will return an array of the first result found.
     * @return Array an array of the first result found in a asscociate array so basically a dictionary.
     */
    public function first() {
        $this->execute();
        return $this->statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * This is the amount of rows found in the query.
     * @return Integer the row count.
     */
    public function count() {
        $this->query($this->queryString);
        $bind = $this->bindAttribs;
        $this->statement->execute($bind);
        return $this->statement->rowCount();
    }

    public function getTableColumns() {
        $this->queryString = "DESCRIBE " . $this->table;
        $this->execute();
        return $this->statement->fetchAll(PDO::FETCH_COLUMN);
    }

    /**
     * This is the where clause this is going to the most used in the class so read this :).
     * @param  String $key      The column of the table you want to compare.
     * @param  String $operator The Comparision operator so =,>,<,LIKE,ect..
     * @param  String $value    The value you want to find in the table in the column specified.
     * @return Object(DB)       I did this because then can attach multiple where caluses or use it with other clauses.
     */
    public function where($key, $operator = '=', $value) {
        $random = substr(md5(microtime()),rand(0,26),3);
        $string = " {$key} {$operator} :{$random}";
        $this->bindAttribs[':' . $random] = $value;
        if($this->whereCount > 0) {
            $this->queryString .= ' OR ' . $string;
        } else {
            $this->whereStatement = 'WHERE ' . $string;
            $this->queryString .= ' WHERE ' . $string;
        }
        $this->whereCount++;
        return $this;
    }
    
    /**
     * This is used to insert into the database.
     * @param  Array  $args An example of this array can be found in the routes.php file.
     */
    public function insert($args = array()) {
        foreach ($args as $arg) {
            $columns = array();
            $values = array();
            foreach ($arg as $key => $value) {
                array_push($columns, $key);
                array_push($values, $value);
            }
            $this->executeInsert($columns, $values);
        }
    }

    /**
     * This is where the insert takes place :).
     * @param  Array  $columns The columns of the table you want to insert into.
     * @param  Array  $values  The values of the columns you want to insert into.
     */
    private function executeInsert($columns = array(), $values = array()) {
        $this->queryString = 'INSERT INTO ' . $this->table;
        $this->queryString .= ' (';
        foreach ($columns as $column) {
            if(end($columns) == $column)  {
                $this->queryString .= $column . ') VALUES (';
            } else {
                $this->queryString .= $column . ',';    
            }    
        }
        foreach ($values as $value) {
            if(end($values) == $value)  {
                $random = substr(md5(microtime()),rand(0,26),3);
                $this->queryString .= ':' . $random . ')';
                $this->bindAttribs[':' . $random] = $value;
            } else {
                $random = substr(md5(microtime()),rand(0,26),3);
                $this->queryString .= ':' . $random . ',';   
                $this->bindAttribs[':' . $random] = $value; 
            }
        }
        $this->execute();
    }

    /**
     * Same as insert but you will get an ID back.
     * @param  Array  $args again in the routes.php file :).
     * @return Integer      The last ID made in the insert.
     */
    public function insertGetId($args = array()) {
        $columns = array();
        $values = array();
        foreach ($args as $key => $value) {
            array_push($columns, $key);
            array_push($values, $value);
        }
        $this->executeInsert($columns, $values);
        return $this->db->lastInsertId();
    }
    
    /**
     * This function will update a row in the table based on the where clause used before this function.
     * @param  Array  $args The arguments can be found in an example found in routes.php.
     */
    public function update($args = array()) {
        $this->queryString = 'UPDATE ' . $this->table . ' SET ';
        if($this->whereCount == 1) {
            foreach ($args as $key => $value) {
                if(end($args) == $value) {
                    $random = substr(md5(microtime()),rand(0,26),3);
                    $this->queryString .= $key . '=:' . $random . ' ';
                    $this->bindAttribs[':' . $random] = $value;
                } else {
                    $random = substr(md5(microtime()),rand(0,26),3);
                    $this->queryString .= $key . '=:' . $random . ',';
                    $this->bindAttribs[':' . $random] = $value;
                }
            }
            $this->queryString .= $this->whereStatement;
            $this->execute();
        } else {
            echo 'Invalid Update statement. Maybe you added more than one WHERE or NONE at all';
        }
    }

    /**
     * To order a result set by a certain column.
     * @param  String $column the column you want to order by in the table.
     */
    public function orderBy($column = 'id') {
        $this->queryString .= ' ORDER BY ' . $column;
        return $this;
    }

    /**
     * If you want it ordered ASC use this.
     */
    public function ASC() {
        $this->queryString .= ' ASC';
        return $this;
    }

    /**
     * If you want it ordered DESC use this.
     */
    public function DESC() {
        $this->queryString .= ' DESC';
        return $this;
    }

    /**
     * Limit the amount of results found
     * @param  Integer $amount The amunt you want to limit it by.
     */
    public function take($amount) {
        $this->queryString .= ' LIMIT ' . $amount;
        return $this;
    }
    
    /**
     * This is used either to deleteAll rows or a certain depending if you use a where clause before the function.
     */
    public function delete() {
        $this->queryString = 'DELETE FROM ' . $this->table;
        if($this->whereCount == 1) {
            $this->queryString .= ' ' . $this->whereStatement;
        } else {
            $this->queryString = 'DELETE * FROM ' . $this->table;
        }
        $this->execute();
    }
}