<?php 

/*

Routes are used to define what the request uri does so what is after the TLD(Top Level Domain) so your .com .co.uk, ect.
there are 2 types of route get and post; Get is used when the request header is get and same with post.

The route is set by defining the request URI so for example "/test",
After that it is a closure and this is called once the route has been found and what ever is returned is displayed on the screen
Things you can return right now are:
String - for example "Test!!!",
View - for example View::make('home')
Image
Response Errors.

Ok, let's explain views for a moment if you want to display a webpage you will need to display a view.
all views are located in the public/views directory

The view is set out by the name of the file so for example home.php and it is just home because I've added the extension for you :)
After that there is a option to put an array of varibles to pass to the view for example array('test(varible name)', 'Test!(Varible Value)')

One big note: remeber to return the view or text or it wont work :'(

Look at all of this work I have to do to help you understand but I love this way, so be it.


 */


/**
 * Home Route it will show the home page.
 */
Route::get('/', 'HomeController@showHome');

/**
 * Test Route
 */
Route::get('/test', function() {
    //DB::table('test')->where('id', '>', 1)->get());
    //DB::table('test')->where('id', '=', 5)->update(['name' => 'John']);
    //DB::table('test')->insert([['name' => 'Ben', 'age' => '16'], ['name' => 'Aaron', 'age' => '17']]);
    //$id = DB::table('test')->insertGetId(['name' => 'Ben', 'age' => '16']);
    //DB::table('test')->delete();
    //DB::table('test')->where('id', '=', 5)->delete();
    //DB::table('feed')->orderBy('PostID')->DESC()->get();
    //DB::sql('SELECT * FROM test')->get();
    return 'Test';
});



Route::post('/', function($request) {
    Session::flash('test', $request->name);
    return View::make('home', ['test' => 'Test123']);
});
