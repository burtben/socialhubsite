<?php

namespace App\http;

use App\http\Routing;

class Route {

    /**
     * Registers a Get Route
     * @param  String   $link    The link after the TLD. For example "/home"  
     * @param  Function $handler The Route handler this is called when the page is loaded.
     */
    public static function get($link, $handler) {
        if(is_callable($handler)) {
            Routing::getInstance()->registerRoute($link, $handler, 'GET');
        } else {
            Routing::getInstance()->registerRouteWithController($link, $handler, 'GET');
        }
    }

    /**
     * Registers a Post Route
     * @param  String   $link    The link after the TLD. For example "/home"  
     * @param  Function $handler The Route handler this is called when the page is loaded.
     */
    public static function post($link, $handler) {
        Routing::getInstance()->registerRoute($link, $handler, 'POST');
    }
}