<?php

namespace App\http;

use Config;
use Response;
use Input;

class Routing {

    private $routes = array();
    private static $instance = null;

    public function __construct() { self::$instance = $this; }

    /**
     * This get's the instance of the class
     * @return Object(Routing) The Routing class will be returned.
     */
    public static function getInstance() { return self::$instance; }

    /**
     * This will check if a registered has been found.
     * @param  String $link   This is the link after the TLD.
     * @param  String $method The method used in the response.
     * @return String         The routes contents.
     */
    public function checkRegisteredRoutes($link = "/", $method="get") {
        foreach ($this->routes as $route) {
            $newLink = $route["link"];
            if(strpos($route["link"], '{') && count(explode('/', $route["link"])) == count(explode('/', $link))) {
                $linkExploded = explode('/', $link);
                $newLink = preg_replace("#({)(.*)(})#si", $linkExploded[count($linkExploded) - 1], $newLink);
                $newLink = str_replace("{", "", $newLink);
                $newLink = str_replace("}", "", $newLink);
                $parameter = $linkExploded[count($linkExploded) - 1];
            }
            if($newLink == $link && $route["method"] == $method) {
                if(isset($route["function"])) {
                    if(isset($parameter)) {
                        if($method == 'POST') {
                            return call_user_func($route["function"], $parameter, Input::all());
                        } else {
                            return call_user_func($route["function"], $parameter);
                        }
                    } else {
                        if($method == 'POST') {
                            return call_user_func($route["function"], Input::all());
                        } else {
                            return call_user_func($route["function"]);
                        }
                    }

                } else {
                    $string = $route["controller"];
                    $parts = explode('@', $string);
                    $controller = 'App\\http\\Controllers\\' . $parts[0];
                    $function = $parts[1];
                    if(isset($parameter)) {
                        $class = new $controller();
                        return $class->$function($parameter);
                    } else {
                        $class = new $controller();
                        return $class->$function();
                    }
                }
            }
        }

        return Response::error(404);
    }

    /**
     * This registers the route to the main Routing class.
     * @param  String   $link    this is the link after the TLD followed by the /.
     * @param  Fucntion $handler route handler.
     * @param  String   $method  the response method used.
     */
    public function registerRoute($link = "/", $handler, $method) {    
        array_push($this->routes, array('link'=> $link, 'function' => $handler, 'method'=> $method));
    } 

    /**
     * This registers the route to the main Routing class.
     * @param  String   $link    this is the link after the TLD followed by the /.
     * @param  Controller $controller route copntroller + function.
     * @param  String   $method  the response method used.
     */
    public function registerRouteWithController($link = "/", $controller, $method) {    
        array_push($this->routes, array('link'=> $link, 'controller' => $controller, 'method'=> $method));
    } 
}