<?php

namespace App\http;

use App\http\Request;
use App\http\Bootstrap;
use App\Config\Config;
use App\http\Routing;
use View;
use File;

class Application {
    private $_request;
    private $_bootstrap;
    private $_router;
    private $_response;
    private $_viewVaribles;
    /**
     * Setup Application.
     */
    public function __construct() {
        //boot system
        $this->_bootstrap = new Bootstrap();
        $this->_bootstrap->boot();

        //Where all the routing takes place :0.
        if(Config::get('routing/enabled')) {
            $this->_router = new Routing();
            include_once(File::find('routes.php'));
            $this->_response = $this->request();
        } else {
            //Load your file here with a require or something...
            echo 'Routing isn\'t enabled, is this a mistake? please contact the system administrator for more information.';
        }
    }
    /**
     * This will return a view or text.
     * @return Object view or text.
     */
    public function display() {
        if(!isset($this->_response)) {
            return;
        }
        if(isset($this->_view)) {
            if(!empty($this->_viewVaribles)) {
                extract($this->_viewVaribles);
            }
            include_once(File::path($this->_response));
        } else {
            echo $this->_response;
        }
    }

    /**
     * This will setup the request system.
     * @return String A view.
     */
    public function request() {
        $this->_request = new Request();
        $response = $this->_request->route($this->_router);
        $type = $response->type;
        $content = $response->content;
        if($content instanceof View) {
            $this->_view = $content->name;
            $this->_viewVaribles = $content->varibles;
            return $content->path;
        } else if(strpos($type, 'image') !== false) {
            $image = imagecreatefromstring($content);
            $imageType = explode("/", $type)[1];
            if($imageType == "jpeg") {
                ob_clean();
                header('Content-Type: image/jpeg');
                imagejpeg($image);
                imagedestroy($image);
            } else if($imageType == "png") {
                ob_clean();
                header('Content-Type: image/png');
                imagepng($image);
                imagedestroy($image);
            } else if($imageType == "gif") {
                ob_clean();
                header('Content-Type: image/gif');
                imagegif($image);
                imagedestroy($image);
            }
        }
    }
}