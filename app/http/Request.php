<?php
namespace App\http;

use Session;
use Response;

 class Request {
    private $ip;
    private $time;
    private $agent;
    private $method;
    private $uri;
    private $previous;
    private $protocol;
    private $port;
    private $self;

    public function __construct() {
        $s = $_SERVER;
        $this->ip = $s['REMOTE_ADDR'];
        $this->time = $s['REQUEST_TIME'];
        $this->agent = $s['HTTP_USER_AGENT'];
        $this->method = $s['REQUEST_METHOD'];
        $this->uri = $s['REQUEST_URI'];
        $this->previous = Session::get('old_request');
        Session::put('old_request', $this->uri);
        $this->protocol = $s['SERVER_PROTOCOL'];
        $this->port = $s['REMOTE_PORT'];
        $this->self = $s['PHP_SELF'];
    }

    public function route($router) {
        $res = $router->checkRegisteredRoutes($this->uri, $this->method);
        if($res instanceof Response) {
            return $res;
        } else {
            return Response::make($res, 200);
        }
    }

 }