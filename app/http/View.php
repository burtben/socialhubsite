<?php 

namespace App\http;

use Config;

class View {

    public $varibles;
    public $path;
    public $name;

    public function __construct($varibles = [], $path, $name) {
        $this->varibles = $varibles;
        $this->path = $path;
        $this->name = $name;
    }

    /**
     * This will create the view and return it.
     * @param  String $file     This is the file name for example: home.
     * @param  array  $varibles Theese are the varibles that will be passed into the view.
     * @return String           This will return the file contents of the files specified.
     */
    public static function make($file, $varibles = array()) {
        return new static($varibles, Config::get('routing/view_directory') . $file . '.php', $file);
    }

    public static function load($path, $varibles = array()) {
        return new static($varibles, $path, 'customPath');
    }

}