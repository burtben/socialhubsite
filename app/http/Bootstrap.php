<?php

namespace App\http;

use App\Config\Config;

class Bootstrap {
    public function boot() {
        //create aliases
        $aliases = Config::get('app/alias');
        foreach ($aliases as $key => $value) {
            class_alias($value, $key);
        }
        new \Auth();
        new \Session();
        new \Input();
    }
}