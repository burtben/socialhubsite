<?php

namespace App\Helpers;

class Password {
    /**
     * This will make a hashed password to go into the DB :).
     * @param  String $password
     * @return String           The hashed password will be returned.
     */
    public static function make($password) {
        $pass = password_hash($password, PASSWORD_BCRYPT, ['cost' => 10]);

        if ($pass === false) {
            throw new RuntimeException('Bcrypt hashing not supported.');
        }

        return $pass;
    }

    /**
     * This will check if the password given was correct to the password hash has given.
     * @param  String $original The non-hashed password
     * @param  String $hashed   The hashed password.
     * @return Boolean
     */
    public static function check($original, $hashed) {
        $check = password_verify($original, $hashed);
        return ($check == 1) ? true : false;
    }
}