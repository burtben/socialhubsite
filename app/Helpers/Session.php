<?php

namespace App\Helpers;

class Session {
    /**
     * This will put a Session into sessions.
     * @param  String $name  The name of the session.
     * @param  Object $value The value of the session.
     */
    public static function put($name, $value) {
        $_SESSION[$name] = $value;
    }
    /**
     * This will remove a session from sessions.
     * @param  String $name name of the session.
     */
    public static function remove($name) {
        unset($_SESSION[$name]);
    }
    /**
     * this will get the value fo the session.
     * @param  String $name name of the session.
     * @return Object       The value of the session.
     */
    public static function get($name) {
        if(isset($_SESSION[$name])) {
            return $_SESSION[$name];
        }
    }

    /**
     * This will flash a message and then once viewed it will be removed.
     * @param  String $name    this is the name of the flash.
     * @param  String $message this is the message you want to display.
     */
    public static function flash($name, $message = "") {
        if($message == "") {
            if(!isset($_SESSION[$name])) {return;}
            $message = self::get($name);
            self::remove($name);
            return $message;
        }
        self::put($name, $message);
    }

}