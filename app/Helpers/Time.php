<?php

namespace App\Helpers;

class Time {
    /**
     * This will return the current time for the system time zone.
     * @return Integer the current time.
     */
    public static function get() {
        return time();
    }

    /**
     * This will print out a UNIX timestamp of the modified time.
     * @param  String $t modify text.
     * @return Integer    UNIX Timestamp.
     */
    private static function modify($t) {
        $tz = new \DateTimeZone('Europe/London');
        $date = new \DateTime("now", $tz);
        $date->modify($t);
        return $date->getTimestamp();
    } 

    /**
     * Add Minutes to the current Time.
     * @param  Integer $amount amount of minutes.
     * @return Integer        UNIX Timestamp.
     */
    public static function minutes($amount) {
        return Time::modify("+" . $amount . " minutes");
    }

    /**
     * Add Days to the current Time.
     * @param  Integer $amount amount of days.
     * @return Integer         UNIX Timestamp.
     */
    public static function days($amount) {
        return Time::modify("+" . $amount . " days");
    }

    /**
     * Add Years to the current Time.
     * @param  Integer $amount amount of years.
     * @return Integer         UNIX Timestamp.
     */
    public static function years($amount) {
        return Time::modify("+" . $amount . " years");
    }

}