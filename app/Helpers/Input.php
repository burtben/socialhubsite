<?php

namespace App\Helpers;

use Session as Sess;

class Input {
    private $_array;
    private $_oldArray;
    private static $in;

    public function __construct() {
        self::$in = $this;
        $this->_array = $_POST;
        $this->_oldArray = Sess::get('old_input');
        Sess::put('old_input', $this->_array);
        foreach($this->_array as $k => $v) {
            $this->$k = $v;
        }
    }

    public static function has($name) {
        if(self::findKey(self::$in->_array, $name)) {
            return true;
        }
        return false;
    }

    public static function all() {
        return self::$in;
    }

    public static function old($name) {
        if(self::findKey(self::$in->_oldArray, $name)) {
            return self::$in->_oldArray[$name];
        }
        return '';
    }

    private static function findKey($array, $keySearch)
    {
        foreach ($array as $key => $item) {
            if ($key == $keySearch) {
                return true;
            }
        }
        return false;
    }

    public static function get($name) {
        if(self::has($name)) {
            return self::$in->_array[$name];
        }
        return '';
    }

}