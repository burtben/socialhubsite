<?php

namespace App\Helpers;

use Config;

class File {
    public static function find($name) {
        $dirs = Config::get('app/directories');
        foreach ($dirs as $dir) {
            $scan = scandir($dir);
            foreach($scan as $file) {
                if($file == $name) {
                    return $dir . $name;
                }
            }
        }
    }

    public static function path($path) {
        return '../' . $path;
    }

    public static function get($path) {
       $file = self::path($path);
       return file_get_contents($file);
    }

    public static function getMime($path) {
        return finfo_file(finfo_open(FILEINFO_MIME_TYPE), self::path($path));
    }
}