<?php

namespace App\Helpers;

use View;
use Config;

class Response {

    public $content;
    public $type = "text/html";

    public function __construct($content = "") {
        $this->content = $content;
    } 

    public static function make($content, $status = 304, $header = "") {
        $in = new static($content);
        if($header != "") {
            $this->type = $header;
            header('Content-Type: ' . $header);
        }
        http_response_code($status);
        return $in;
    }

    public static function error($code) {
        http_response_code($code);
        return new static(View::load(Config::get('routing/view_directory') . 'errors/' . $code . '.php'));
    }

    public function header($value) {
        ob_clean();
        header('Content-Type: ' . $value);
        $this->type = $value;
    }

    public function responseCode($code) {
        http_response_code($code);
    }
}