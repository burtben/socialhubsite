<?php

namespace App\Helpers;

class Cookie {

    /**
     * This will return the value of a cookie.
     * @param  String $name name of the cookie.
     * @return String       value of cookie.
     */
    public static function value($name) {
        return $_COOKIE[$name];
    }

    /**
     * This will delete the cookie.
     * @param  String $name name of cookie.
     */
    public static function delete($name) {
        setcookie($name, "", time() - 1000000);
    }

    /**
     * This will create a cookie.
     * @param  String $name this is the name of the cookie.
     * @param  String $data this is the data stored on the cookie.
     * @param  Integer $time this is a UNIX Timestamp to tell when the cookie will expire, use the Time class for this.
     */
    public static function create($name, $data, $time) {
        setcookie($name, $data, $time);
    }

}