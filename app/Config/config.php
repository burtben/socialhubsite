<?php

/*

This file is proberly the simplest of all the classes :).
Config::get((filename)/(dictonary_key));

so for example...

Config::get('routing/enabled');
That will return true because in the routing file it is true :).

 */

namespace App\Config;

class Config {
    /**
     * Gets the String based on the key specified
     * @param  String $key e.g. database/host
     * @return String       e.g. localhost
     */
    public static function get($key) {
        $parts = explode('/', $key);
        $array = self::loadConfigFile($parts[0]);
        $data = $array;
        for($i = 1; $i < count($parts); $i++) {
            if(isset($data[$parts[$i]])) {
                $data = $data[$parts[$i]];
            }
        }
        return $data;
    }
    /**
     * Get the config file we need instead of loading them all :0.
     * @param  String $file filename e.g. database
     * @return Array       this is the array returned from the file.
     */
    private static function loadConfigFile($file) {
        return require $file . '.php';
    }
}