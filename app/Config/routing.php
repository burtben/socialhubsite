<?php

return [
    // Do you want routing enabled?
    'enabled' => true,
    //This is where all your views are located and where the system will look for them.
    'view_directory' => 'public/views/',
];