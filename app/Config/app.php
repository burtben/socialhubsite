<?php

return [

    /*
    I have only just really learned how this works. So basically it turns the class into a shortened version :)
    I don't expect you ro know how namespaces work but if you want you look it up.
     */

    'alias' => array(
        'Config' => App\Config\Config::class,
        'View' => App\http\View::class,
        'Route' => App\http\Route::class,
        'DB' => App\Database\DB::class,
        'Password' => App\Helpers\Password::class,
        'Cookie' => App\Helpers\Cookie::class,
        'Time' => App\Helpers\Time::class,
        'Auth' => App\Helpers\Auth::class,
        'Session' => App\Helpers\Session::class,
        'Response' => App\Helpers\Response::class,
        'File' => App\Helpers\File::class,
        'Input' => App\Helpers\Input::class
    ),

    'directories' => [
        '../app/',
        '../app/http/',
        '../app/http/Controllers/',
        '../app/Config/',
        '../app/Database/',
        '../app/Database/models/',
        '../app/Helpers/'
    ]

];