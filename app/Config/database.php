<?php

return [
    //Host Adress e.g. 127.0.0.1
    'host' => 'localhost',
    //Username e.g. root
    'username' => 'homestead',
    //Password e.g. root
    'password' => 'secret',
    //Databse e.g. database
    'database' => 'socialhub'
];