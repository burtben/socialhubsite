<?php

/*

Ok, If you are wondering how this all works, this is the page where everybody lands when the webpage is requested.

Step 1: Session is started like in most webpages :)
Step 2: Then we include the autoloader file which grabs the classes from app/ and app/http and when we request a class the autoload will find the class.
Step 3: then we initialize the Routing class and assign it the routing varible.
Step 4: we include the routes file to understand how they work read the stuff I put in there basically it's where you register routes.
Step 5: we then check if a registered route has the same uri and method as the ones we pass in.
Step 6: then we check if a view has been made if it had any varibles to be made.
Step 7: then we finally echo out the response made by the route :).

Hopefully this helped you I know it's alot to take in at once, but I have tryed to document all of my functions to help you further understand what is going on behind the scenes.

If you would like to find the file that is being displayed when the uri is just simply / then goto the public/views folder and it's the home.php file :). If you wanna know more about how routes work then goto the routes.php file and I explain it there. 


 */
session_start();
include_once('helpers.php');
include_once('../app.php');



