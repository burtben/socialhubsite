
<?php
/*Directories that contain classes*/
$classesDir = array (
    '../app/',
    '../app/http/',
    '../app/http/Controllers/',
    '../app/Config/',
    '../app/Database/',
    '../app/Database/models/',
    '../app/Helpers/',
);
function __autoload($class_name) {
    global $classesDir;
    foreach ($classesDir as $directory) {
        $parts = explode('\\', $class_name);
        if (file_exists($directory . end($parts) . '.php')) {
            require_once ($directory . end($parts) . '.php');
            return;
        }
    }
}

require_once '../resources/kint/Kint.php';

function dd($dump) {
    Kint::$theme = 'solarized-dark';
    array_map(function($x) { Kint::dump($x); }, func_get_args());
    die;
}